var rootUrl = 'http://localhost:8999/',
    divs = document.querySelectorAll("[ag-include]");


var rendorForIn = function (resp) {
    var tmpNode = document.createElement('div');
    tmpNode.innerHTML = resp;
    var tmp = tmpNode.querySelectorAll('[ag-for]');
};

var getTemplate = function (url,cb) {
    fetch(url, {
        method: 'GET', cache: 'force-cache'
    }).then(function(response){
        return response.text()
    }).then(function (resp) {
        cb(resp)
    });
};

[].forEach.call(divs,function (div,ind) {
    getTemplate([rootUrl , 'template/' , div.getAttribute('ag-include'), '.html'].join(''),function (resp) {
        rendorForIn(resp);
        div.innerHTML = resp;
        if(ind===divs.length-1){
            window.socket = io.connect('http://localhost:8999');
            window.socket.on('message', function(msg){
                document.getElementById('active-client').innerText = 'No. of active client:'+msg.clients;
            });
        }
    });
});