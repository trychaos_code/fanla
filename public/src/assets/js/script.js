var msgArr = {t:[],h:[]};
var ag = {
    data:{},
    browser:{
        list:{

        },
        selected:{

        }
    }
};

ag.rendorNMsg = function (n,end) {
    if(msgArr.t.length){
        var slc = document.querySelector("#process-status");
        slc.innerHTML = msgArr.t.slice(end-n>=0?end-n:0,end).join('');
    }else {
        slc.innerText = 'Nothing processed yet'
    }
    slc.scrollTop = slc.scrollHeight
};

ag.analyse = function (typ) {

    var inp = document.querySelectorAll("input.form-input");
    [].forEach.call(inp,function (i) {
        if(i.value){
            ag.data[i.getAttribute('name')] = i.value;
        }
    });

    window.socket.emit(typ ==='git'?'analyse_git':'analyse_local',ag.data);
    window.socket.on('ana-resp', function(msg){
        msgArr.t.push('<p>'+ msg.msg+'</p>');
        ag.rendorNMsg(100,msgArr.t.length-1);
    });
};