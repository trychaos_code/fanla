### Image optimisation

It builds Optimised images in output/public folder

_**Commands Options**_

node myTask.js 

--search= Path of your parent directory where your HTML are present(It can automatically find sub-directory)

--option= jsMin (For JS Minification & Concatenation Traversing through HTML)


--vendor= Vendor folder name (default values will be node_modules, bower_components)

--vendor= multiple Vendor folder can be provided


_Note:_ output, vendor, version are optional
  
 
 _Sample command_
 
 node myTask.js --search=/Users/akhilesk/Desktop/fAnl/server/testDir --option=traverse  --vendor=node_modules --vendor=bower_components


 node myTask.js --search=.\cuic\isuite\cuic-components --option=traverse

